package com.myrotiuk.model

case class Targeting(targetedSiteIds: LazyList[String])

case class Banner(id: Int, src: String, width: Int, height: Int)

case class Campaign(id: String, country: String, targeting: Targeting, banners: List[Banner], bid: Double)

case class CampaignBannerDTO(id: String, banner: Banner)
