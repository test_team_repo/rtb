package com.myrotiuk.web

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{pathEnd, pathPrefix, post, _}
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import com.myrotiuk.web.CampaignRegistry.{ActionPerformed, GetBid, SimulateStorage}

import scala.concurrent.Future

class CampaignRoutes(campaignRegistry: ActorRef[CampaignRegistry.Command])(implicit val system: ActorSystem[_]) {

  import JsonFormats._
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  private implicit val timeout = Timeout.create(system.settings.config.getDuration("rtb.routes.ask-timeout"))

  def getCampaign(request: BidRequest): Future[BidResponse] =
    campaignRegistry.ask(GetBid(request, _))

  def simulateCampaigns(): Future[ActionPerformed] =
    campaignRegistry.ask(SimulateStorage)

  val campaignRoutes: Route =
    concat(
      pathPrefix("campaigns") {
        pathEnd {
          post {
            entity(as[BidRequest]) { request =>
              onSuccess(getCampaign(request)) { response =>
                complete((StatusCodes.OK, response))
              }
            }
          }
        }
      },
      pathPrefix("simulate") {
        pathEnd {
          post {
            onSuccess(simulateCampaigns()) { response =>
              complete((StatusCodes.OK, response))
            }
          }
        }
      }
    )
}
