package com.myrotiuk.web

import com.myrotiuk.model.Banner
import com.myrotiuk.web.CampaignRegistry.ActionPerformed
import spray.json.DefaultJsonProtocol

object JsonFormats  {
  import DefaultJsonProtocol._

  implicit val bannerJsonFormat = jsonFormat4(Banner)
  implicit val siteJsonFormat = jsonFormat2(Site)
  implicit val geoJsonFormat = jsonFormat1(Geo)
  implicit val userJsonFormat = jsonFormat2(User)
  implicit val deviceJsonFormat = jsonFormat2(Device)
  implicit val impressionJsonFormat = jsonFormat8(Impression)

  implicit val bidRequestJsonFormat = jsonFormat5(BidRequest)
  implicit val bidResponseJsonFormat = jsonFormat5(BidResponse)
  implicit val actionPerformedJsonFormat = jsonFormat1(ActionPerformed)
}
