package com.myrotiuk.web
import util.Random.shuffle

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.myrotiuk.model.{Banner, Campaign, CampaignBannerDTO, Targeting}
import com.myrotiuk.tree.RtbKdTreeMap

import scala.util.Random

object CampaignRegistry {

  sealed trait Command
  final case class GetBid(request: BidRequest, replyTo: ActorRef[BidResponse]) extends Command
  final case class SimulateStorage(replyTo: ActorRef[ActionPerformed]) extends Command

  final case class ActionPerformed(description: String)

  def apply(): Behavior[Command] = registry(new RtbKdTreeMap())

  private def registry(storage: RtbKdTreeMap): Behavior[Command] =
    Behaviors.receiveMessage {
      case GetBid(request, replyTo) =>
        val response: BidResponse = getCampaign(request, storage)
        replyTo ! response
        Behaviors.same
      case SimulateStorage(replyTo) =>
        val newStorage = simulate(storage)
        replyTo ! ActionPerformed(s"Simulated dataset")
        registry(newStorage)
    }

  private def simulate(storage: RtbKdTreeMap) = {
    shuffle(('A' to 'J').toList).map( country =>
      shuffle(('p' to 'y').toList).map(siteId =>
        shuffle((200 to 300 by 10).toList).map(width =>
          shuffle((150 to 250 by 10).toList).map(heigth =>
            shuffle((1 to 10).toList).map(bidFloor =>
              storage.addCampaign(Campaign(Random.alphanumeric.take(10).mkString, country.toString, Targeting(LazyList(siteId.toString)),
                List(Banner(Random.nextInt(), "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", width, heigth)), bidFloor))
            )
          )
        )
      )
    )
    storage.addCampaign(Campaign(Random.alphanumeric.take(10).mkString, "LT", Targeting(LazyList("0006a522ce0f4bbbbaa6b3c38cafaa0f")),
      List(Banner(Random.nextInt(), "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", 300, 250)), 5d))
  }

  private def getCampaign(request: BidRequest, storage: RtbKdTreeMap): BidResponse = {
    val maybeCountry = request.device.map(_.geo).orElse(request.user.map(_.geo)).flatMap(geo => geo.flatMap(_.country))
    val foundCampaignDTOs: List[(Double, Option[CampaignBannerDTO])] = request.imp.map(imp =>
      (imp.bidFloor, storage.getCampaign(request.site.id, maybeCountry, imp.bidFloor, imp.wmin, imp.wmax, imp.w, imp.hmin, imp.hmax, imp.h))
    )
    val possibleMatches = foundCampaignDTOs.map(priceDto => {
      val (price, maybeDto) = priceDto
      BidResponse(java.util.UUID.randomUUID.toString, request.id, price, maybeDto.map(_.id), maybeDto.map(_.banner))
    })
    val response: BidResponse = possibleMatches.find(_.adid.isDefined).getOrElse(possibleMatches.head)
    response
  }
}
