package com.myrotiuk.web

import com.myrotiuk.model.Banner

case class Geo(country: Option[String])
case class User(id: String, geo: Option[Geo])
case class Device(id: String, geo: Option[Geo])
case class Site(id: String, domain: String)
case class Impression(id: String, wmin: Option[Int], wmax: Option[Int], w: Option[Int], hmin: Option[Int], hmax: Option[Int], h: Option[Int], bidFloor: Double)
case class BidRequest(id: String, imp: List[Impression], site: Site, user: Option[User], device: Option[Device])

case class BidResponse(id: String, bidRequestId: String, price: Double, adid: Option[String], banner: Option[Banner])