package com.myrotiuk.tree

import com.myrotiuk.model.{Campaign, CampaignBannerDTO}
import com.myrotiuk.tree.RtbKdTreeMap._

import scala.annotation.tailrec

protected final case class Node(targetedSiteId: String, country: String, bid: Double, width: Int, height: Int)

protected final case class Entry(node: Node, campaign: CampaignBannerDTO, var left: Option[Entry] = None, var right: Option[Entry] = None)

class RtbKdTreeMap(var root: Option[Entry] = Option.empty) {
  def addCampaign(campaign: Campaign): RtbKdTreeMap = {
    val nodeDTOs: LazyList[(Node, CampaignBannerDTO)] = campaign.targeting.targetedSiteIds.flatMap(targetedSiteId =>
      campaign.banners.map(banner =>
        (Node(targetedSiteId, campaign.country, campaign.bid, banner.width, banner.height),
          CampaignBannerDTO(campaign.id, banner))
      )
    )
    val iterator = nodeDTOs.iterator
    while (iterator.hasNext) {
      val (node, dto) = iterator.next()
      addEntryToTree(Entry(node, dto))
    }
    this
  }

  private def addEntryToTree(newEntry: Entry): Unit = {
    root match {
      case Some(entry) => addEntryToTreeAtDimension(newEntry, entry)
      case None => root = Some(newEntry)
    }
  }

  @tailrec
  private def addEntryToTreeAtDimension(newEntry: Entry, parent: Entry, dimension: Int = 0): Unit = {
    val entryNode = newEntry.node
    val parentNode = parent.node

    val nextDimension = dimension + 1
    dimension match {
      case 0 => addTreeNode(newEntry, parent, nextDimension, entryNode.targetedSiteId < parentNode.targetedSiteId)
      case 1 => addTreeNode(newEntry, parent, nextDimension, entryNode.country < parentNode.country)
      case 2 => addTreeNode(newEntry, parent, nextDimension, entryNode.bid.compareTo(parentNode.bid) < 0)
      case 3 => addTreeNode(newEntry, parent, nextDimension, entryNode.width < parentNode.width)
      case 4 => addTreeNode(newEntry, parent, nextDimension, entryNode.height < parentNode.height)
      case _ => addEntryToTreeAtDimension(newEntry, parent)
    }
  }

  private def addTreeNode(newEntry: Entry, parent: Entry, nextDimension: Int, perDimensionComparator: Boolean) = {
    if (perDimensionComparator) {
      parent.left match {
        case Some(leftChild) => addEntryToTreeAtDimension(newEntry, leftChild, nextDimension)
        case None =>
          parent.left = Some(newEntry)
      }
    } else {
      parent.right match {
        case Some(rightChild) => addEntryToTreeAtDimension(newEntry, rightChild, nextDimension)
        case None =>
          parent.right = Some(newEntry)
      }
    }
  }

  def getCampaign(targetedSiteId: String, country: Option[String], bidFloor: Double,
                  wmin: Option[Int], wmax: Option[Int], w: Option[Int], hmin: Option[Int], hmax: Option[Int], h: Option[Int]): Option[CampaignBannerDTO] =
    getCampaignAtDimension(
      (cSiteId: String) => siteIdComparator(targetedSiteId, cSiteId),
      (cCountry: String) => countryComparator(country, cCountry),
      (cBid: Double) => bidComparator(bidFloor, cBid),
      (cWidth: Int) => sizeComparator(wmin, wmax, w, cWidth),
      (cHeight: Int) => sizeComparator(hmin, hmax, h, cHeight),
      root
    )

  @tailrec
  private def getCampaignAtDimension(siteIdMatcher: String => Comparison, countryMatcher: String => Comparison, bidMatcher: Double => Comparison,
                                     widthMatcher: Int => Comparison, heightMatcher: Int => Comparison, optionParent: Option[Entry], dimension: Int = 0): Option[CampaignBannerDTO] = {
    optionParent match {
      case None => None
      case Some(parent) =>
        val parentNode = parent.node

        val siteIdComparison = siteIdMatcher(parentNode.targetedSiteId)
        val contryComparison = countryMatcher(parentNode.country)
        val bidComparison = bidMatcher(parentNode.bid)
        val widthComparison = widthMatcher(parentNode.width)
        val heightComparison = heightMatcher(parentNode.height)

        val matchFound = siteIdComparison == Equal &&
          (contryComparison == Equal || contryComparison == Undefined) &&
          (bidComparison == Equal || bidComparison == Undefined) &&
          (widthComparison == Equal || widthComparison == Undefined) &&
          (heightComparison == Equal || heightComparison == Undefined)

        if (matchFound) {
          Some(parent.campaign)
        } else {
          val nextDimension = dimension + 1
          dimension match {
            case 0 => getResultFromSubtreeBasedOnComparison(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent, siteIdComparison, nextDimension)
            case 1 => getResultFromSubtreeBasedOnComparison(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent, contryComparison, nextDimension)
            case 2 => getResultFromSubtreeBasedOnComparison(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent, bidComparison, nextDimension)
            case 3 => getResultFromSubtreeBasedOnComparison(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent, widthComparison, nextDimension)
            case 4 => getResultFromSubtreeBasedOnComparison(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent, heightComparison, nextDimension)
            case _ => getCampaignAtDimension(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, optionParent)
          }
        }
    }
  }

  private def getResultFromSubtreeBasedOnComparison(siteIdMatcher: String => Comparison, countryMatcher: String => Comparison, bidMatcher: Double => Comparison,
                                                    widthMatcher: Int => Comparison, heightMatcher: Int => Comparison, parent: Entry,
                                                    currentComparison: Comparison, nextDimension: Int): Option[CampaignBannerDTO] =
    currentComparison match {
      case Less => getCampaignAtDimension(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent.left, nextDimension)
      case Undefined | Equal =>
        val leftSubtreeResult = getCampaignAtDimension(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent.left, nextDimension)
        val rigSubtreeResult = getCampaignAtDimension(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent.right, nextDimension)
        (leftSubtreeResult, rigSubtreeResult) match {
          case (None, None) => None
          case (None, r) => r
          case (l, _) => l
        }
      case _ => getCampaignAtDimension(siteIdMatcher, countryMatcher, bidMatcher, widthMatcher, heightMatcher, parent.right, nextDimension)
    }
}

object RtbKdTreeMap {
  def countryComparator(optionCountry: Option[String], cCountry: String): Comparison =
    optionCountry match {
      case Some(country) =>
        if (country > cCountry) Greater
        else if (country < cCountry) Less
        else Equal
      case None => Undefined
    }

  def siteIdComparator(targetedSiteId: String, cSiteId: String): Comparison =
    if (targetedSiteId > cSiteId) Greater
    else if (targetedSiteId < cSiteId) Less
    else Equal

  def bidComparator(bidFloor: Double, cBid: Double): Comparison =
    if (bidFloor.compareTo(cBid) <= 0) Equal else Greater

  def sizeComparator(optionMin: Option[Int], optionMax: Option[Int], absolute: Option[Int], cSize: Int): Comparison =
    absolute match {
      case Some(size) =>
        if (size > cSize) Greater
        else if (size < cSize) Less
        else Equal
      case None =>
        (optionMin, optionMax) match {
          case (Some(min), Some(max)) =>
            if (cSize >= min && cSize <= max) Equal
            else if (min > cSize) Greater
            else Less
          case (Some(min), None) =>
            if (min <= cSize) Equal
            else Greater
          case (None, Some(max)) =>
            if (max >= cSize) Equal
            else Less
          case _ => Undefined
        }
    }
}

sealed trait Comparison

protected case object Less extends Comparison

protected case object Greater extends Comparison

protected case object Equal extends Comparison

protected case object Undefined extends Comparison
