package com.myrotiuk.tree

import com.myrotiuk.model.{Banner, Campaign, Targeting}
import org.scalatest.flatspec._
import org.scalatest.matchers._
import org.scalatest.prop.TableDrivenPropertyChecks

import scala.util.Random

class RtbKdTreeMapTest extends AnyFlatSpec with should.Matchers with TableDrivenPropertyChecks {

  "RtbKdTree add campaign" should "have empty root" in {
    val testedInstance = new RtbKdTreeMap()
    testedInstance.root shouldBe empty
  }

  it should "add root entry for empty com.myrotiuk.tree" in {
    val testedInstance = new RtbKdTreeMap()
    val targeting = Targeting(LazyList("0006a522ce0f4bbbbaa6b3c38cafaa0f"))
    val banners = List(Banner(1, "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", 300, 250))
    val campaign = Campaign(Random.alphanumeric.take(10).mkString, "LT", targeting, banners, 5d)
    testedInstance.addCampaign(campaign)
    testedInstance.root should not be empty
    testedInstance.root.get.campaign.id should be(campaign.id)
  }

  it should "add n entries for each banner" in {
    val testedInstance = new RtbKdTreeMap()
    val targeting = Targeting(LazyList("0006a522ce0f4bbbbaa6b3c38cafaa0f"))
    val banners = List(Banner(1, "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", 300, 250),
      Banner(2, "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", 301, 251))
    val campaign = Campaign(Random.alphanumeric.take(10).mkString, "LT", targeting, banners, 5d)
    testedInstance.addCampaign(campaign)
    testedInstance.root should not be empty
    testedInstance.root.get.right should not be empty
    testedInstance.root.get.right.get.campaign.id should be(campaign.id)
  }

  it should "add n entries per each targetedSiteId" in {
    val testedInstance = new RtbKdTreeMap()
    val targeting = Targeting(LazyList("0006a522ce0f4bbbbaa6b3c38cafaa0f", "0006a522ce0f4bbbbaa6b3c38cafaa0e"))
    val banners = List(Banner(1, "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", 300, 250))
    val campaign = Campaign(Random.alphanumeric.take(10).mkString, "LT", targeting, banners, 5d)
    testedInstance.addCampaign(campaign)
    testedInstance.root should not be empty
    testedInstance.root.get.left should not be empty
    testedInstance.root.get.left.get.campaign.id should be(campaign.id)
  }

  it should "add child nodes on left and right side of root element" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("R", "LT", 5d, 300, 250))

    testedInstance.root should not be empty
    testedInstance.root.get.left should not be empty
    testedInstance.root.get.right should not be empty

    testedInstance.root.get.node.targetedSiteId should be("M")
    testedInstance.root.get.right.get.node.targetedSiteId should be("R")
    testedInstance.root.get.left.get.node.targetedSiteId should be("H")
  }

  it should "add child nodes on left and right side from second node" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("L", "KE", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "MG", 5d, 300, 250))

    testedInstance.root should not be empty
    val hSiteId = testedInstance.root.get.left
    hSiteId should not be empty

    testedInstance.root.get.node.targetedSiteId should be("M")
    hSiteId.get.node.targetedSiteId should be("H")

    hSiteId.get.left should not be empty
    hSiteId.get.right should not be empty

    hSiteId.get.left.get.node.country should be("KE")
    hSiteId.get.right.get.node.country should be("MG")
  }

  it should "add child nodes on left and right side from third node" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("L", "KE", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 6d, 300, 250))

    testedInstance.root should not be empty
    val hSiteId = testedInstance.root.get.left
    hSiteId should not be empty
    val lKe = hSiteId.get.left
    lKe should not be empty
    lKe.get.left should not be empty
    lKe.get.right should not be empty
    lKe.get.left.get.node.bid should be(3d)
    lKe.get.right.get.node.bid should be(6d)
  }

  it should "add child nodes on left and right side from forth node" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("L", "KE", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 250, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 350, 250))

    testedInstance.root should not be empty
    val hSiteId = testedInstance.root.get.left
    hSiteId should not be empty
    val lKe = hSiteId.get.left
    lKe should not be empty
    lKe.get.left should not be empty
    val kGw3dw300 = lKe.get.left.get
    kGw3dw300.left should not be empty
    kGw3dw300.right should not be empty
    kGw3dw300.left.get.node.width should be (250)
    kGw3dw300.right.get.node.width should be (350)
  }

  it should "add child nodes on left and right side from fifth node" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("L", "KE", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 250, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 280, 200))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 280, 300))

    testedInstance.root should not be empty
    val hSiteId = testedInstance.root.get.left
    hSiteId should not be empty
    val lKe = hSiteId.get.left
    lKe should not be empty
    lKe.get.left should not be empty
    val kGw3dw300 = lKe.get.left.get
    kGw3dw300.left should not be empty
    val kGw3dw250 = kGw3dw300.left.get
    kGw3dw250.left should not be empty
    kGw3dw250.right should not be empty
    kGw3dw250.left.get.node.height should be (200)
    kGw3dw250.right.get.node.height should be (300)
  }
  it should "add child nodes on left and right side from sixth node" in {
    val testedInstance = new RtbKdTreeMap()

    testedInstance.addCampaign(getCampaign("M", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("H", "LT", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("L", "KE", 5d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 300, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 250, 250))
    testedInstance.addCampaign(getCampaign("K", "GW", 3d, 280, 200))
    testedInstance.addCampaign(getCampaign("J", "GW", 3d, 280, 240))
    testedInstance.addCampaign(getCampaign("L", "GW", 3d, 280, 240))

    testedInstance.root should not be empty
    val hSiteId = testedInstance.root.get.left
    hSiteId should not be empty
    val lKe = hSiteId.get.left
    lKe should not be empty
    lKe.get.left should not be empty
    val kGw3dw300 = lKe.get.left.get
    kGw3dw300.left should not be empty
    val kGw3dw250 = kGw3dw300.left.get
    kGw3dw250.left should not be empty
    val kGw3dw250h200 = kGw3dw250.left.get

    kGw3dw250h200.left should not be empty
    kGw3dw250h200.right should not be empty

    kGw3dw250h200.left.get.node.targetedSiteId should be("J")
    kGw3dw250h200.right.get.node.targetedSiteId should be("L")
  }

  "siteIdComparator" should "return correct comparison results" in {
    val givenExpected = Table(
      ("targetedSiteId", "campaignSiteId", "expectedResult"),
      ("g", "h", Less),
      ("h", "g", Greater),
      ("g", "g", Equal)
    )
    forAll(givenExpected) { (targetedSiteId: String, campaignSiteId: String, expectedResult: Comparison) =>
      RtbKdTreeMap.siteIdComparator(targetedSiteId, campaignSiteId) shouldBe expectedResult
    }
  }

  "countryComparator" should "return correct comparison results" in {
    val givenExpected = Table(
      ("targetedCountry", "campaignCountry", "expectedResult"),
      (None, "LT", Undefined),
      (Some("g"), "h", Less),
      (Some("h"), "g", Greater),
      (Some("g"), "g", Equal)
    )
    forAll(givenExpected) { (targetedCountry: Option[String], campaignCountry: String, expectedResult: Comparison) =>
      RtbKdTreeMap.countryComparator(targetedCountry, campaignCountry) shouldBe expectedResult
    }
  }

  "bidComparator" should "return correct comparison results" in {
    val givenExpected = Table(
      ("targetedBid", "campaignBid", "expectedResult"),
      (1d, 3d, Equal),
      (5d, 3d, Greater),
      (3d, 3d, Equal)
    )
    forAll(givenExpected) { (targetedBid: Double, campaignBid: Double, expectedResult: Comparison) =>
      RtbKdTreeMap.bidComparator(targetedBid, campaignBid) shouldBe expectedResult
    }
  }

  "sizeComparator" should "return correct comparison results" in {
    val givenExpected = Table(
      ("optionMin", "optionMax", "absolute", "campaignSize", "expectedResult"),
      (None, None, None, 200, Undefined),
      (None, None, Some(200), 200, Equal),
      (None, None, Some(250), 200, Greater),
      (None, None, Some(180), 200, Less),
      (Some(180), None, None, 200, Equal),
      (Some(200), None, None, 200, Equal),
      (Some(210), None, None, 200, Greater),
      (None, Some(200), None, 200, Equal),
      (None, Some(220), None, 200, Equal),
      (None, Some(180), None, 200, Less)
    )
    forAll(givenExpected) { (optionMin: Option[Int], optionMax: Option[Int], absolute: Option[Int], campaignSize: Int, expectedResult: Comparison) =>
      RtbKdTreeMap.sizeComparator(optionMin, optionMax, absolute, campaignSize) shouldBe expectedResult
    }
  }

  "RtbKdTree find campaign" should "return None for newly created instance" in {
    val testedInstance = new RtbKdTreeMap()
    val result = testedInstance.getCampaign("h", country = None, bidFloor = 1d, wmin = None, wmax = None, w = None, hmin = None, hmax = None, h = None)
    result shouldBe empty
  }

  it should "return root element if match created campaign" in {
    val testedInstance = new RtbKdTreeMap()
    val campaign = getCampaign("M", "LT", 5d, 300, 250)
    testedInstance.addCampaign(campaign)
    val result = testedInstance.getCampaign("M", country = None, bidFloor = 1d, wmin = None, wmax = None, w = None, hmin = None, hmax = None, h = None)
    result should not be empty
    result.get.id shouldBe (campaign.id)
  }

  it should "return appropriate campaign" in {
    val testedInstance = new RtbKdTreeMap()

    val campaign0 = getCampaign("M", "LT", 5d, 300, 250)
    testedInstance.addCampaign(campaign0)
    val campaign1 = getCampaign("H", "LT", 5d, 300, 250)
    val campaign2 = getCampaign("O", "LT", 5d, 300, 250)
    testedInstance.addCampaign(campaign1)
    testedInstance.addCampaign(campaign2)
    val campaign3 = getCampaign("L", "KE", 5d, 300, 250)
    val campaign4 = getCampaign("L", "MG", 5d, 300, 250)
    testedInstance.addCampaign(campaign3)
    testedInstance.addCampaign(campaign4)
    val campaign5 = getCampaign("K", "GW", 3d, 300, 250)
    val campaign6 = getCampaign("K", "GW", 6d, 300, 250)
    testedInstance.addCampaign(campaign5)
    testedInstance.addCampaign(campaign6)
    val campaign7 = getCampaign("K", "GW", 3d, 250, 250)
    val campaign8 = getCampaign("K", "GW", 3d, 350, 250)
    testedInstance.addCampaign(campaign7)
    testedInstance.addCampaign(campaign8)
    val campaign9 = getCampaign("K", "GW", 3d, 280, 200)
    val campaign10 = getCampaign("K", "GW", 3d, 280, 300)
    testedInstance.addCampaign(campaign9)
    testedInstance.addCampaign(campaign10)
    val campaign11 = getCampaign("J", "GW", 3d, 280, 240)
    val campaign12 = getCampaign("L", "GW", 3d, 280, 240)
    testedInstance.addCampaign(campaign11)
    testedInstance.addCampaign(campaign12)

    val givenExpected = Table(
      ("targetedSiteId", "country", "bidFloor", "wmin", "wmax", "w", "hmin", "hmax", "h", "expectedResult"),
      ("M", Some("LT"), 5d, None, None, Some(300), None, None, Some(250), Some(campaign0.id)),
      ("H", Some("LT"), 5d, None, None, Some(300), None, None, Some(250), Some(campaign1.id)),
      ("O", Some("LT"), 5d, None, None, Some(300), None, None, Some(250), Some(campaign2.id)),
      ("L", Some("KE"), 5d, None, None, Some(300), None, None, Some(250), Some(campaign3.id)),
      ("L", Some("MG"), 5d, None, None, Some(300), None, None, Some(250), Some(campaign4.id)),
      ("K", Some("GW"), 3d, None, None, Some(300), None, None, Some(250), Some(campaign5.id)),
      ("K", Some("GW"), 6d, None, None, Some(300), None, None, Some(250), Some(campaign6.id)),
      ("K", Some("GW"), 3d, None, None, Some(250), None, None, Some(250), Some(campaign7.id)),
      ("K", Some("GW"), 3d, None, None, Some(350), None, None, Some(250), Some(campaign8.id)),
      ("K", Some("GW"), 3d, None, None, Some(280), None, None, Some(200), Some(campaign9.id)),
      ("K", Some("GW"), 3d, None, None, Some(280), None, None, Some(300), Some(campaign10.id)),
      ("J", Some("GW"), 3d, None, None, Some(280), None, None, Some(240), Some(campaign11.id)),
      ("L", Some("GW"), 3d, None, None, Some(280), None, None, Some(240), Some(campaign12.id)),

      ("Y", Some("LT"), 5d, None, None, Some(300), None, None, Some(250), None),
      ("H", Some("JS"), 5d, None, None, Some(300), None, None, Some(250), None),
      ("O", Some("LT"), 10d, None, None, Some(300), None, None, Some(250), None),
      ("L", Some("KE"), 5d, None, None, Some(100), None, None, Some(250), None),
      ("L", Some("MG"), 5d, None, None, Some(300), None, None, Some(100), None),

      ("K", Some("GW"), 3d, None, None, Some(280), Some(210), None, None, None),
      ("K", Some("GW"), 3d, None, None, Some(280), None, Some(180), None, None),
      ("K", Some("GW"), 3d, None, None, Some(280), Some(180), Some(210), None, Some(campaign9.id)),
      ("K", Some("GW"), 3d, None, None, Some(280), None, Some(210), None, Some(campaign9.id)),
      ("K", Some("GW"), 3d, None, None, Some(280), Some(180), None, None, Some(campaign9.id)),
      ("K", Some("GW"), 3d, None, None, Some(280), None, None, None, Some(campaign9.id)),

      ("L", Some("GW"), 3d, Some(200), Some(350), None, None, None, Some(240), Some(campaign12.id)),
      ("L", Some("GW"), 3d, None, None, None, None, None, Some(240), Some(campaign12.id)),
      ("L", Some("GW"), 3d, None, Some(300), None, None, None, Some(240), Some(campaign12.id)),
      ("L", Some("GW"), 3d, Some(200), None, None, None, None, Some(240), Some(campaign12.id)),
      ("L", Some("GW"), 3d, Some(281), None, None, None, None, Some(240), None),
      ("L", Some("GW"), 3d, None, Some(278), None, None, None, Some(240), None),
    )

    forAll(givenExpected) { (targetedSiteId: String, country: Option[String], bidFloor: Double,
                             wmin: Option[Int], wmax: Option[Int], w: Option[Int], hmin: Option[Int], hmax: Option[Int], h: Option[Int], expectedResult: Option[String]) =>
      testedInstance.getCampaign(targetedSiteId, country, bidFloor, wmin, wmax, w, hmin, hmax, h).map(dto => dto.id) shouldBe expectedResult
    }
  }

  "Example test" should "pass" in {
    val testedInstance = new RtbKdTreeMap()
    val campaign = getCampaign("0006a522ce0f4bbbbaa6b3c38cafaa0f", "LT", 5d, 300, 250)
    testedInstance.addCampaign(campaign)
    val result = testedInstance.getCampaign("0006a522ce0f4bbbbaa6b3c38cafaa0f", Option("LT"), 3.12123d, Option(50), Option(300), Option(300), Option(100), Option(300), Option(250))
    result.map(dto => dto.id) shouldBe (Some(campaign.id))
  }

  private def getCampaign(siteId: String, country: String, bid: Double, width: Int, height: Int): Campaign = {
    val targeting = Targeting(LazyList(siteId))
    val banners = List(Banner(1, "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", width, height))
    val campaign = Campaign(Random.alphanumeric.take(10).mkString, country, targeting, banners, bid)
    campaign
  }
}
