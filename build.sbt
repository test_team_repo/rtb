lazy val akkaHttpVersion = "10.2.1"
lazy val akkaVersion = "2.6.10"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.myrotiuk",
      scalaVersion := "2.13.4",
    )),
    name := "rtb",
    version := "0.1",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "org.scalactic" %% "scalactic" % "3.2.2",
      "org.scalatest" %% "scalatest" % "3.2.2" % Test
    )
  )
