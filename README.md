Current solution is build using KdTreeMap data structure for effective matching logic.
Core search/matching logic is covered with tests.
Additional test and improvements may be added to validate input.
Integration tests are omitted at the moment.

Possible improvements:
Adding Bloom filter may help defining that element is not present amount campaigns. 

`/simulate` endpoint is added to validate the solution. This endpoint populates search tree with 10^5
campaigns (may be adjusted) which may be used for querying.
```
shuffle(('A' to 'J').toList).map(country =>
  shuffle(('p' to 'y').toList).map(siteId =>
    shuffle((200 to 300 by 10).toList).map(width =>
      shuffle((150 to 250 by 10).toList).map(heigth =>
        shuffle((1 to 10).toList).map(bidFloor =>
          storage.addCampaign(Campaign(Random.alphanumeric.take(10).mkString, country.toString, Targeting(LazyList(siteId.toString)),
            List(Banner(Random.nextInt(), "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", width, heigth)), bidFloor))
        )
      )
    )
  )
)
```
